require ethercat
require IsegThq2He60,catane
require ethercatcea,catane

requireExec(scanner, "$(REQUIRE_IsegThq2He60_PATH)/misc/IsegThq2He60_ethercatScanner.xml -q /tmp/ethercatIsegThq2He60", "", "/tmp/ethercatIsegThq2He60")
sleep 10
ecAsynInit("/tmp/ethercatIsegThq2He60", 100000)

############################################################
################### Chargement records  ####################
############################################################

dbLoadTemplate(IsegThq2He60.substitutions)

#**********************************************************#
#*********************** INIT  ****************************#
#**********************************************************#

iocInit

