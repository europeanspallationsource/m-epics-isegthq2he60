include $(EPICS_ENV_PATH)/module.Makefile

STARTUPS = startup/IsegThq2He60.cmd
DOC = doc/README.md
MISCS= misc/IsegThq2He60_ethercatScanner.xml

USR_DEPENDENCIES=ethercatcea,catane
vpath % ../../misc
